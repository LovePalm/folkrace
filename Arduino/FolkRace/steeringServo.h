// steeringServo.h

#ifndef _STEERINGSERVO_h
#define _STEERINGSERVO_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <PWMServo.h>
class steeringServo : public PWMServo
{
 protected:

 public:
	steeringServo(uint8_t pin); 
	void init();

	int16_t setSteeringAngleDeg(int16_t angleDeg); 

private:
  // Neutral = straight forward
  // Max right = 125
  // Max left = 55
	enum angles {
		neutral = 90,
		maxRight = neutral +30,
		maxLeft = neutral -30
	};	
};



#endif
