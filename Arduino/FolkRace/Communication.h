// Communication.h

#ifndef _COMMUNICATION_h
#define _COMMUNICATION_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "Supervisor.h"
#include "globals.h"

/** \defgroup Communication Communication
 *  @{
 */

/** Handles communication with other units. Currently used to handle communication over bluehtooth and wire. TODO: We could probably use some sort of inheritance from the serial port classes here.    */
namespace Communication
{
	/** Polls all connected serial buses for commands. */
	int pollSerial();

	/** A simple message queue template for incoming values. Currently the queue is only one element long, and we 
	always only use the latest value written. Say we get a new parameter for the PID, then this
	type of message can be used to set the PID parameters.*/
	template <typename T>
	class valueSetMessage
	{
	public:
		/** Assign new value to the queue, flag a new value has been received, and
		can call a proper callback set function if assigned.  */
		void set(const T& _value)
		{
			value = _value;
			hasNewVal = true;

			if (valueSettingCallBack != nullptr)
			{
				valueSettingCallBack(_value);
			}
		}
		/** Return and remove latest element */
		T pop()
		{
			return value;
			hasNewVal = false;
		}

		/** Return latest element but do not remove it from the queue. */
		T get()
		{
			return value; 
		}

		bool hasNewValue()
		{
			return hasNewVal; 
		}

		/** A callback that sets the passes on the incoming message when arriving.    */
		void(*valueSettingCallBack)(T) = nullptr;

	private:
		T value;
		bool hasNewVal = false;
	};
	
	/** Do a loopback test. Add this to the mainloop and call it with an argument such as "Serial1". All messages on Serial1 should echo back now. Make 
	sure no other program unit is already reading the port. */
	void loopBackTest(HardwareSerial& port); 
	
	extern Supervisor::mainMode currentMainModeRequest;  

	namespace incomingParameters
	{
		extern valueSetMessage<float> Kp; 
	}

	namespace serialCommand
	{
		enum commands
		{

			RUN = 'r',
			STOP = 's',
			TUNE = 't',
			FORWARD = 'f'
		};
	}

	/*Anon. namespace defining private members.*/
	namespace
	{
		/** Parse through a Stream and look for commands etc.*/
		//extern int parseSerial(Stream& busToParse); //TODO: Now it is a "loose" function in the source file. 
	}

};


/** @}*/



#endif

