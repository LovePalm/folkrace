// ESC.h

#ifndef _ESC_h
#define _ESC_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <PWMServo.h>

/** @brief
	A standard ESC for an RC car uses the same signal standard as many servos. So we inherit all functions from the 
	PWM servo class, and add a few functions we need in the context of ESC motor control. 

*/

class ESC :public PWMServo
{
 protected:


 public:
	ESC(uint8_t PIN); 
	void init();
	void setVel(int8_t vel );
	enum direction {
		forward = 1,
		reverse = 0
	};
	int16_t calibrateESC(direction dir, uint8_t val); 

	/** Basically do deadband compensation by finding the signals where the ESC reverses/forwards
	TODO: Does it make sense to have it as an enum? */

	enum velMap {
		neutral = 72,
		forwardOffset = 88, /*Where motor starts to reverse*/
		backwardOffset = 65, /*Where motor start to move forwards*/
	};

private: 


};

//extern ESC ESC;

#endif

