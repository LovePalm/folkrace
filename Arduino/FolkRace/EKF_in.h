#pragma once
/*to initialize some EKF functions*/

#ifndef EKF_IN_H
#define EKF_IN_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "pins.h"
#include "IRsensor.h"

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

IRsensor frontSensor(PIN::frontSensor);
IRsensor rearSensor(PIN::rearSensor);

static void argInit_1x4_front_sensor(double result[4])
{
	//double result_tmp;
	result[0] = -25;
	result[1] = 110;
	result[2] = 0.8232;
	result[3] = 100;
}

static void argInit_1x4_rear_sensor(double result[4])
{
	//double result_tmp;
	result[0] = -25;
	result[1] = 32;
	result[2] = 0.0;
	result[3] = 100;
}

double get_prior_mean()
{
	double x_0 = 0.0;
	double sum_x = 0;
	int count = 0;
	for (int i = 0; i < 100; i++)
	{
		x_0 = 1000 * rearSensor.readDistance();
		sum_x = sum_x + x_0;
		count++;
	}
	x_0 = sum_x / 100;
	return x_0;


}


#endif