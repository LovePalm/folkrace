/**********************************************************************************************
 * Arduino PID Library - Version 1.1.1
 * by Brett Beauregard <br3ttb@gmail.com> brettbeauregard.com
 *
 * This Library is licensed under a GPLv3 License
 **********************************************************************************************/

#if ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

#include "PID.h"

/*Constructor (...)*********************************************************
 *    The parameters specified here are those for for which we can't set up 
 *    reliable defaults, so we need to have the user set them.
 ***************************************************************************/
PID::PID(float __Kp, float __Ki, float __Kd, float __maxOut, float Ts/*=0*/, float Tf/*=0*/)
: Kp(__Kp), Ki(__Ki), Kd(__Kd), maxOut(__maxOut)
{
	//Calculate constant for first order lowpass filter 
	if (Tf > 0 && Ts > 0)
	{
		alpha = Ts / (Ts + Tf); 
		useFilter = true; 
	}
	else
	{
		useFilter = false; 
	}

	//Set this conditions so we dont waste so much time calculating it. 
	if (__Kp != 0) useP = true; 
	else useP = false; 

	if (__Ki !=0 ) useI = true;
	else useI = false;

	if (__Kd != 0) useD = true;
	else useD = false;

	reset(); 
}

float PID::update(const float& setPoint, const float& processValue, const float& Ts/*=0*/)
{
	float error = setPoint - processValue; 
	
	
	//Time difference since last function call 
	float deltaT = (float(micros()) - lastMicros)*0.000001F;
	
	//////////////////////////////////////////////
	//PROPORTIONAL
	Pout = Kp*error;

	
	///////////////////////////////////////////
	///DERIVATIVE with low pass filter 
	if (useD)
	{
		if (useFilter)
		{
			float filteredError = lastFilteredError*(1 - alpha) + alpha*error; 

			Dout = Kd*(filteredError - lastFilteredError) / deltaT; 

			lastFilteredError = filteredError; 

		}
		else
		{
			Dout = (error - lastFilteredError) / deltaT; 
			lastFilteredError = error; 
		}

	}
	
	///////////////////////////////////////////////////////
	//INTEGRATING with anti-windup by clamping. 
	if ( abs(Iout + Pout + Dout) < maxOut )
	{
		Iout += deltaT*Ki*error; 
	}
	
	/////////////////////////////////////////////
	//COMPLETE output, add clamping too
	out = Pout + Iout + Dout; 

	if (out > maxOut) {
		out = maxOut;
	}
	else if (out < -maxOut) {
		out = -maxOut;
	}

	lastMicros = float(micros()); 

	return out; 
}



//Most importanly resets the interators, that can spin off like crazy during startup. 
void PID::reset()
{
	lastFilteredError = 0; 
	lastMicros = micros(); 
	Iout = 0; 
}

float PID::tuneKp(const float& processValue, const float& deadBand)
{
	float error = -processValue; 
	tuneCnt++; 

	if (tuneCnt % 100 == 0)
	{
		Kptune += 0.1; 

		Serial.print("Using tuning Kp: " );
		Serial.println(Kptune); 

	}

	if (abs(processValue) < deadBand) error = 0; 

	return error*Kptune; 

}

void PID::setKp(float _Kp)
{
	Kp = _Kp; 
}

 

