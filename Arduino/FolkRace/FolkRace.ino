
#include "motorDriver.h"
#include "Communication.h"
#include "Supervisor.h"
#include "pins.h"
#include "IRsensor.h"
#include "IIR_firstOrder.h"
#include "movingAverage.h"
#include "steeringServo.h"
//#include "ESC.h"
//#include "PWMservo/PWMServo.h"
#include <PWMServo.h>
#include "globals.h"
#include "PID.h"
#include "pins.h"
#include "Supervisor.h"
#include "Communication.h"
#include "motorDriver.h"
#include <Wire.h>
#include "MedianFilter.h"
#include <stddef.h>
#include <stdlib.h>
#include "EKF.h"
#include "EKF_in.h"



//movingAverage<float, 20> filter; 
//MedianFilter med_filter(30, 0);
float Kp_ds =0.09;
float Ki_ds = 0;
//float Kd_ds = 0.1;
float Kd_ds = 0;
float maxOut = 50; //This is steering angle in degees in this case. 
PID wallController(Kp_ds, Ki_ds, Kd_ds, maxOut); //PID output signal here is steering angle in degrees. 
IIR_firstOrder filter(0.6); //Argument is tuning factor, closer to unity means "more" filtering

motorDriver motor(PIN::motorForward, PIN::motorReverse); 
steeringServo steering(PIN::steeringServo); 
//ESC motor(PIN_motor); 
Supervisor supervisor; 

//IRsensor frontSensor(PIN::frontSensor); //defined in EKF_in.h
//IRsensor rearSensor(PIN::rearSensor);


boolean blueToothActivated = true; //Flag to view serial information through Telemetry or Serial Monitor

void setup() 
{
  // initialize the digital pin as an output.
  pinMode(PIN::led, OUTPUT);
  digitalWrite(PIN::led, HIGH);

  motor.init(); 
  blueTooth.begin(9600);
  wireSerial.begin(9600);

  //Decide if we want to send messages to both bluetooth and wire. 
  
  steering.init();

  EKF_initialize(get_prior_mean());
  //delay(500);
}

// the loop() methor runs over and over again,
// as long as the board has power

int16_t		PWM_value = 0;

uint32_t	lastMicros = 0;

float		ds_set_mm = 200; 

float		steeringAngle_deg = 0;
int			forwardSpeed = 0;

uint32_t	elapsedTime_us = micros(); 
const uint32_t	Ts_us = 1000 * 20; //20 ms control loop update. 
char*	formatTelemetryData();


float		ds_front_tmp;
float		ds_rear_tmp;
double		wallSensorParams_front_tmp[4];
double		wallSensorParams_rear_tmp[4];
double		d_hat;
double		theta_hat;
const double	Ts = 0.02;
//TODO: Why doesnt these functions show up in doxygen?? 
/** \defgroup Main Main
Main functionalities of the program. 
 *  @{
 */

 /**@brief The main loop single task where all main functions calls are done. */
void loop() 
{
 
  /** In order to get deterministic sampling time, we poll the sensors and controller at a given rate. 
     TODO: - Install RTOS? 
            - Verify sampling rate polling a pin and using an oscilloscope      */
  elapsedTime_us = micros() - lastMicros;

  /**************************Incoming communication***********************
   Most of communication consists of serial port polling
   *****************************************************************/
  
  //Communication::loopBackTest(blueTooth);

  /** Poll main mode from serial port. Currently just a start stop button basically. */
  if (Communication::pollSerial()>0)
  {
	supervisor.setMainMode(Communication::currentMainModeRequest);

	using namespace Communication; 

	if (incomingParameters::Kp.hasNewValue())
	{
		wallController.setKp(incomingParameters::Kp.pop()); 
	}
  }
  
  if (elapsedTime_us > Ts_us )
  {
	lastMicros = micros();


    /******************* Control functions *********************
     Read sensors, update output etc. 
     ***********************************************************/
     
	ds_front_tmp = 1000 * frontSensor.readDistance(); //raw distance from sensor
	ds_rear_tmp = 1000 * rearSensor.readDistance();

	/*******************EKF filter***************************/
	/* Initialize function input argument 'wallSensorParams_front'. */
	argInit_1x4_front_sensor(wallSensorParams_front_tmp);
	argInit_1x4_rear_sensor(wallSensorParams_rear_tmp);
	  
	EKF(ds_front_tmp, ds_rear_tmp, Ts, wallSensorParams_front_tmp,
		  wallSensorParams_rear_tmp, &d_hat, &theta_hat);
    /* Set the value to positive in case of two possible solutions */
    d_hat = abs(d_hat);
    /********************************************************/

    /*****************Median filter************************/
    /********Median filter with windiw size of 7***********/
	//sensor_raw = leftSensor.ADCraw; 
	
	//med_filter.in(ds_rear_tmp);
	//d_hat = med_filter.out();
    /******************************************************/
    //sensor_filt = filter.update(float(sensor_dist_raw));
    //sensor_filt = max(sensor_filt, 100); //To prevent faulty values being sent as a consequence of the IR-sensor giving false values when being closer than 100 mm.
    
    //TODO: Appropriate action whenever the sensor value from the distance sensor is invalid. We have to change sign on both due to the definition of steering angle. Note the minus sign!
	/** \f[ \delta_i = K_p(d_s-d_{set})] */

    //TODO: Implement a proper speed algorihm
    // Do a simple Voltage read to compensate the set speed signal to the motors:
    

    //At around 100 is starts to move (depending on the surface!)
    //Maximum is 254 (we will probably not need to exceed 200 for the course, but lets see!)
	
	switch (supervisor.getMainMode())
	{
	case Supervisor::RUNNING:
		/* Steering P controller and the motor speed*/
		steeringAngle_deg = -wallController.update(ds_set_mm, d_hat);
		steering.setSteeringAngleDeg(int16_t(steeringAngle_deg));
		PWM_value = (130 - 1 * abs(steeringAngle_deg));
		motor.setPWM(PWM_value);
		break;

	case Supervisor::IDLE:
		steeringAngle_deg = -wallController.update(ds_set_mm, d_hat);
		PWM_value = 0;
		motor.setPWM(PWM_value);
		break;

	case Supervisor::FORWARD:
		steeringAngle_deg = -wallController.update(ds_set_mm, d_hat);
		PWM_value = 130;
		motor.setPWM(PWM_value);
		break;

	}

    /************ Outgoing communication ************  
    Wait until the serial port buffer is free until sending data. Else the print function
    might create an unecessarily large delay.

    https://www.pjrc.com/teensy/td_uart.html
    
    ***********************************/

	char* telemetryData = formatTelemetryData(); 

	if (wireSerial.availableForWrite() > 10)
	{
		//wireSerial.println(telemetryData);
    Serial.print(d_hat);
    Serial.print("\t");
    Serial.println(ds_rear_tmp);
	}

    if (blueToothActivated) 
	{
		if (blueTooth.availableForWrite() > 10)
		{
			blueTooth.println(telemetryData);
		}
    }
  }
}
/** @}*/

void SerialPrint(boolean blueToothActivated, String textName, float val, int flagEnd)
{
  if (blueToothActivated == false) {
    //Serial.print(textName);
    //Serial.print(val);
    //Serial.print("; ");
  
    if (flagEnd == 1)
    {
      //Serial.println();
    }
  }
}


/** Format data for Telemetry viewer. */
char text[43];
char* formatTelemetryData()
{
	//Automatically generated code from telemetryviewer. 
	int timestamp = millis(); //TODO: Should be uint
	snprintf(text, 43, "%d,%d,%d,%d,%d,%d,%d", timestamp, int(ds_rear_tmp), int(d_hat), int(ds_set_mm), int(steeringAngle_deg), 0,0);

	return text; 
}
