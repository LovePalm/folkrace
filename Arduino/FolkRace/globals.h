#pragma once



/** \defgroup Globals Globals
 *  @{
 */
/** In case we change the physical connection here later. This is basically the control switch which directs
all serial communication to the defined port. TODO: Maybe would be better or at least safer using some inline wrapper. */
#define blueTooth	Serial2//Bluetooth is connected to Serial2 . 
#define wireSerial	Serial 

/**Convert non SI values to SI  */
namespace fromNonSItoSI
{
	/**From m.m. to meters. Use like 100*mm_ (returns meters) */
	const float mm_ = 0.001;  

}

/** @}*/