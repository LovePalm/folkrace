// 
// 
// 

#include "Supervisor.h"

Supervisor::Supervisor()
{
	currentMainmode = IDLE; 
}

Supervisor::mainMode Supervisor::setMainMode(mainMode request)
{
	//TODO: Safety checks etc for mainmode request if the state machine grows. 
	currentMainmode = request; 

	if (currentMainmode != lastMainMode)
	{
		blueTooth.print("Setting mainmode to: ");
		blueTooth.println(mainModeToString());
	}

	lastMainMode = currentMainmode; 
}

String Supervisor::mainModeToString()
{
	switch (currentMainmode) {
	case IDLE: 
		return "Idle"; 
	case RUNNING: 
		return "Running"; 
	case FORWARD:
		return "fORWARD";
	}; 

	return "Unknown"; 
}
Supervisor::mainMode Supervisor::getMainMode() {

	return currentMainmode; 
}


