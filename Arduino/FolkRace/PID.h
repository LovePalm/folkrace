#ifndef PID_h
#define PID_h

/* PID by
Martin P�r-Love Palmhttp://mechatronicsblogger.blogspot.com
   Integration and derivation done with seconds as unit. 
*/


/** \defgroup Libraries Libraries
 *  @{
 */


/**@brief A basic PID controller. TODO: Implement an antiwindup for the integrator. Also the compact formulation for when sampling time is known.*/
class PID
{
	public:
	
		/**@param Ts Controller sampling interval 
		   @param Tf  Time constant for single pole filter on derivative
		   @param maxOut Saturation on control signal +/- maxOut*/
		PID(float Kp, float Ki, float Kd, float maxOut, float Ts =0, float Tf =0 );
		
		void reset(); 

		//Increment Kp with a setpoint zero until the system is marginally stable. 
		float tuneKp(const float& processValue, const float& deadBand); 

		void setKp(float _Kp);
		
		float out, Iout, Pout, Dout; 

		float lastFilteredError; 

		/** Calculate the new pid controller output given current setpoint and process value. */
		float update(const float& setPoint, const float& processValue, const float& Ts=0);

	private:
		float lastMicros; 
		float Kp, Ki, Kd;
		float Kptune = 0; 
		uint16_t tuneCnt = 0; 
		const float maxOut; 
		float deadBand = 0; 

		bool useFilter, useD, useI, useP; 
		
		float alpha; 

};

/** @}*/
#endif

