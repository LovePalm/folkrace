// movingAverage.h

#ifndef _MOVINGAVERAGE_h
#define _MOVINGAVERAGE_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
#include "QueueArray.h"

/**
@brief Moving average filter 
Implemented by shifting only the last values to a queue. Output at current time index k is calculated according to 
  \f[y_k = \frac{1}{N}\sum_{i=k-N+1}^{k} u_i \f]

*/

template<typename T, uint16_t size>
class movingAverage 
{
 public:
	 /**
	 Updates filter output y by shifting in and out last and current value according to 

	 \f[y_k = y_{k-1} + \frac{u_k}{N} - \frac{u_{k-N}}{N}    \f]


	 @param input Input to the filter, eg. latest measurement value. 
	 */
	 T update(T input) 
	 {
		 T weightedValue = T(float(input) / float(size));
		 output += weightedValue; 

		 if (queue.isFull()) 
		 {
			 output -= queue.dequeue(); 
		 }	
		 queue.enqueue(weightedValue);
		 return output; 
	 }
	

private: 
	QueueArray<T, size> queue; /** Contains all values for calculating the moving average*/
	T output;                        
	
};



#endif

