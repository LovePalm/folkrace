// 
// 
// 

#include "steeringServo.h"



steeringServo::steeringServo(uint8_t pin)
{
	attach(pin); 
}

void steeringServo::init()
{
	setSteeringAngleDeg(0);
	delay(100); 
}

/*Set the steering angle of the wheels with respect to the chassis. When this angle is around 0 the 
servo, with the super class PWMservo sees around 90 degrees. TODO: Should probably contain a non linear map*/
int16_t steeringServo::setSteeringAngleDeg(int16_t angleDeg)
{
	int16_t angleToWrite = angles::neutral; 

	if (angleDeg !=0)
	{
		angleToWrite = angles::neutral - angleDeg; //Minussign for the current coordinate system assumption to be correct. 
	}
	
	//Clamp/saturate angle 
	if (angleToWrite > angles::maxRight)
	{
		angleToWrite = angles::maxRight; 
	}
	else if (angleToWrite < angles::maxLeft)
	{
		angleToWrite = angles::maxLeft;
	}

	//Write desired angle to servo 
	write(angleToWrite); 

	return angleToWrite; 
}
