// 
// 
// 

#include "Communication.h"



Supervisor::mainMode Communication::currentMainModeRequest = Supervisor::mainMode::IDLE;
Communication::valueSetMessage<float> Communication::incomingParameters::Kp;
 

//TODO: Define this in the anon. namespace of communication 
int parseSerial(Stream& busToParse )
{
	using namespace Communication;
	int bytesRead = 0;


	

	while (busToParse.available())
	{
	
		bytesRead++;

		switch (busToParse.read())
		{
		case serialCommand::RUN:
			currentMainModeRequest = Supervisor::mainMode::RUNNING;
			break;
		case serialCommand::STOP:
			currentMainModeRequest = Supervisor::mainMode::IDLE;
			break;
		case serialCommand::FORWARD:
			currentMainModeRequest = Supervisor::FORWARD;
			break;
		case serialCommand::TUNE:
			/*Wait for incoming values. Whenever there are more values we want to tune we can also 
			check for a subspeficier here. */
			uint8_t maxSize = 30;
			char buffer[maxSize];
			uint8_t id = 0;

			blueTooth.println("Enter a value. ");
			
			/* While this would make sense, it seems that many streams use overrides of this function so it doesnt work. 
			busToParse.println("Enter a value");
			*/

			/* Wait for at least one byte to arrive, and wait some more for further bytes to arrive in case the user wants to set a value longer than a digit,
			that we might miss due to communication delays.. 
			Then, parse this value to a float and send it to the corresponding
				buffer.
			*/
			while (busToParse.available() < 1) {};
			delay(100);

			while (busToParse.available() && id < maxSize - 1)
			{
				buffer[id++] = busToParse.read();
			}

			float readValue = atof(buffer);
			//TODO: Debug messages should be sent on the busToParse, however didnt work with the Serial2 class for some reason, probably println for it is overriden from stream. 
			blueTooth.println("I got value");
			blueTooth.println(readValue);
			Communication::incomingParameters::Kp.set(readValue);
			
			//Assume this is a non time critical application and use the delay to hold the debug messages for a while. 
			delay(1000); 

			break;
		};
	}

	return bytesRead;
}
	



		
int Communication::pollSerial()
{

	int bytesRead = 0;
	
	bytesRead += parseSerial(blueTooth);
	bytesRead += parseSerial(wireSerial);

	return bytesRead;
}

void Communication::loopBackTest(HardwareSerial & port)
{
	if (!port.available()) {
		return;
	}
	else {
		port.print("Reveived: ");
		port.println( port.readString());
	}
}
