// pins.h

#ifndef _PINS_h
#define _PINS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

/** \addtogroup Globals 
 *  @{
 */

/**@brief All pin configurations of the program.  */
namespace PIN
{

	const int	led = 13;
	const int	steeringServo = 6;
	const int	motorForward = 2;
	const int	motorReverse = 3;
	const int	frontSensor = A13;
	const int	rearSensor = A14;
	const int	batteryVoltage = A0; //Is it?
}

/** @}*/

#endif

