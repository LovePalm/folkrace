// Supervisor.h

#ifndef _SUPERVISOR_h
#define _SUPERVISOR_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
//#include "Communication.h"
#include "globals.h"

/** \defgroup Supervisor Supervisor
 *  @{
 */

/**@brief The supervisor has overview of the whole program and sets the current main mode. TODO: implement safe mode switches etc. */
class Supervisor
{	
public: 
	Supervisor();

	enum mainMode{
		IDLE,
		RUNNING,
		FORWARD
	};

	/**Poll serial bus for start/stop command. */
	mainMode setMainMode(mainMode request);

	mainMode getMainMode(); 

	String mainModeToString(); 

private: 
	mainMode currentMainmode = IDLE; 
	mainMode lastMainMode = IDLE; 

};

/** @}*/
#endif

