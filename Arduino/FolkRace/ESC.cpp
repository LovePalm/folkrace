// 
// 
// 

#include "ESC.h"

ESC::ESC(uint8_t PIN)
{
	attach(PIN); 
}

void ESC::init()
{	
	//We have to write the neutral value, then a bit forward and wait for a while before the ESC activates. It is a safety function. 
	//Specific for HPI EN2 esc. 
	write(velMap::neutral); 
	delay(2000); 

	//This weird stuff works for some reason. 
	for (int vel = velMap::neutral; vel < velMap::neutral +5 ; vel ++){
		write(vel);
		delay(500); 
	}
	write(velMap::neutral); 
	

}

void ESC::setVel(int8_t vel)
{
	if (vel > 0){
		write(forwardOffset + vel); 

	}
	else if(vel < 0) {
		write(backwardOffset + vel); 

	}
	else {
		write(velMap::neutral);
	}

}

//Find the values where ESC starts to move the motor. We do not have any velocity feedback so you have to monitor the return value.
//Might not make a heapload of sense but anyway. 
int16_t ESC::calibrateESC(direction dir, uint8_t val)
{
	switch (dir){
		case direction::forward:
			val++; 
			break; 


		case direction::reverse:
			val--; 
			break;
	}
	write(val); 
	delay(1000); 
	return val; 
}



