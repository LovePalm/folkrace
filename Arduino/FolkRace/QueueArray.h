/*
 *  QueueArray.h
 *
 *  Library implementing a generic, dynamic queue (array version).
 *
 *  ---
 *
 *  Copyright (C) 2010  Efstathios Chatzikyriakidis (contact@efxa.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *  ---
 *
 *    2016-12-05 - Martin P�r-Love Palm 
		Implemented static allocation for safer behaviour in embedded system applications. 
 *  Version 1.0
 *
 *    2014-02-03  Brian Fletcher  <brian.jf.fletcher@gmail.com>
 *
 *      - added enqueue(), dequeue() and front().
 *
 *    2010-09-29  Efstathios Chatzikyriakidis  <contact@efxa.org>
 *
 *      - added resize(): for growing, shrinking the array size.
 *
 *    2010-09-25  Efstathios Chatzikyriakidis  <contact@efxa.org>
 *
 *      - added exit(), blink(): error reporting and handling methods.
 *
 *    2010-09-24  Alexander Brevig  <alexanderbrevig@gmail.com>
 *
 *      - added setPrinter(): indirectly reference a Serial object.
 *
 *    2010-09-20  Efstathios Chatzikyriakidis  <contact@efxa.org>
 *
 *      - initial release of the library.
 *
 *=============================================================================
   The original library uses dynamic allocation. For the sake of a application that is safety critical and needs to be
   highly deterministic, the library was rewritten to use static allocation. This by 

   http://mechatronicsblogger.blogspot.com 

   for the autonomous boat project Follow Me 2016
 *
 *  For the latest version see: http://www.arduino.cc/
 */

// header defining the interface of the source.
#ifndef _QUEUEARRAY_H
#define _QUEUEARRAY_H

/// the definition of the queue class.

template<typename T, uint16_t size>
class QueueArray {
public:
	// init the queue (constructor).
	QueueArray(){
		items = 0;      // set the number of items of queue to zero.
		head = 0;       // set the head of the queue to zero.
		tail = 0;       // set the tail of the queue to zero.
	}

	/*If the queue was empty and the caller tried to pop(), empty data is returned. Should probably
	be remade though so it looks up the pointer to emptyinstance rader*/
	bool isEmptyInstance(T check) const
	{
		return emptyInstance == check; 
	}

	/*If anything was changed in the queue, this function will return differently. Can be used to see
	if the que was altered TODO: Check if this always work, or if there are cases that could go undetected*/
	uint32_t getState() const
	{
		return items + head + tail; 
	}

	/*Adds an item to the queue, if there is place left. Returns true if succesful*/
	bool enqueue(const T i){
		// check if the queue is full.
		if (isFull()) return false; 

		// Store the item into the array. In C/C++, the assignment does a deep copy of a whole object 
		contents[tail++] = i;

		// wrap-around index.
		if (tail == size) tail = 0;

		// increase the items.
		items++;

		return true; 
	}

	/*Returns the front item from the queue and then removes it from the queue*/
	T dequeue()
	{
		// check if the queue is empty.
		if (! isEmpty() ) 
		{
			// fetch the item from the array.
			T item = contents[head++];

			// decrease the items.
			items--;

			// wrap-around index.
			if (head == size) head = 0;

			// return the item from the array.
			return item;
		
		}
		else
		{
			/*TODO: PRINT message*/
			return emptyInstance;  //THIS WILL FAIL??
		}

	}

	/*Clears all data from the queue*/
	void clear()
	{
		items = 0; 
		head = 0; 
		tail = 0; 
	}

	// push an item to the queue.
	void push(const T i)
	{
		enqueue(i);
	}

	// pop an item from the queue.
	T pop()
	{
		return dequeue();
	}

	/*Is a read only of the front of the queue, which is the next element to be popped or dequeued*/
	T front() const
	{
		// check if the queue is empty.
		if (isEmpty()) 
		{/*TODO: PRINT WARNING*/ 
			return emptyInstance; 
		}
		else
		{ 
			// get the item from the array.
			return contents[head];
		}

	}

	/*Is a read only of the back of the queue, which is the last element that was put there*/
	T back() const
	{
		// check if the queue is empty.
		if (isEmpty()) 
		{/*TODO: PRINT WARNING*/ 
			return emptyInstance; 		
		}
		else
		{
			uint16_t index;
			//The tail is always pointing to the next value to be written, so we have to go back one step and also 
			//avoid a wraparound
			if (tail == 0) index = size - 1;
			else		   index = tail - 1;

			// get the item from the array.
			return contents[index];
		}

	}


	/*Is a read only of the front of the queue, which is the next element to be popped or dequeued*/
	T peek() const
	{
		return front();
	}

	// check if the queue is empty.
	bool isEmpty() const
	{
		return items == 0;
	}

	// get the number of items in the queue.
	int count() const
	{
		return items;
	}

	// check if the queue is full.
	bool isFull() const
	{
		return items == size;
	}

private:
	/*Returned when the queue is empty*/
	T emptyInstance;

	T  contents[size];    /* The array of the queue, containing all its data*/

	uint16_t items;			// the number of items of the queue.

	uint16_t head;			// the head of the queue.
	uint16_t tail;			// the tail of the queue.
};

#endif // _QUEUEARRAY_H
