/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * EKF.h
 *
 * Code generation for function 'EKF'
 *
 */
#ifdef __cplusplus
extern "C" {
#endif

#ifndef EKF_H
#define EKF_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
//#include "EKF_types.h"




/* Function Declarations */
extern void EKF(float ds_front, float ds_rear, const double Ts, const double
                wallSensorParams_front[4], const double wallSensorParams_rear[4], double* d_hat, double* theta_hat);

extern void EKF_init(double prior_mean);


extern void EKF_terminate(void);


extern void EKF_initialize(double prior_mean);


#endif

#ifdef __cplusplus
}
#endif
/* End of code generation (EKF.h) */
