Telemetryviewer is a neat program for plotting values live from the COM port. Run "RunTelemetryViewer.bat", connect to the COM port used to send the data and load the layout file inside this folder.

TODO: Get telemetryviewer to load a certain layout as default during startup. 