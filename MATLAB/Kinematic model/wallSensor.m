function [ds] = wallSensor(d, heading, sensorParams)
% Simulate a distance sensor measuring the distance to a wall. 

%Wall sensor. A sensor located at xs, ys in the body frame is meauring the
%distance ds to the walw. The wall in this case is the y axis. 
%
%
psi = heading; 
xs = sensorParams.xs; 
ys = sensorParams.ys; 
alpha_s = sensorParams.alpha_s; 

%We get no detections if we are too close to the wall, or if 
% the heading is so large that we are actually reversing.
tol = deg2rad(5); 
if (abs(alpha_s-psi )) < pi/2-tol && abs(d)>abs(xs)
    ds = (d+xs*cos(psi)-ys*sin(psi))/(cos(alpha_s-psi));
else
    ds=0; 
end
end

