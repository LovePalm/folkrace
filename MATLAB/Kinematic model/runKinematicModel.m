
clc; 
clear all;
close all;   

addpath(genpath('.')); %Add all subfolders to path. 
addpath('../Sensors')
%Simulink is way more strictly typed than MATLAB and does not seem to 
% accept simple incorporation of struct values passed from matlab. 
% Therefore, we define all structs used in both MATLAB and simulink as 
% Bus objects, that are loaded from this file. 
load 'busDefinitions'

%% Simple kinematic model of a vehicle. 
%It is in many aspects similar to https://se.mathworks.com/matlabcentral/fileexchange/54852-simple-2d-kinematic-vehicle-steering-model-and-animation
%but we use another coordinate system and a simpler animation. 
%Also, we use a more exact rep. of ackerman angles instead of the
%approximation
% tan(steeringangle) ~steeringAngle. 

L = 5; %Length between front wheel axis 
B = 2.2; %Track width

%% Conversions 
kmh_ = 1/3.6; %Kmh to m/s 

%% Track generation
%Generate a simple track. 

%track_straightLine;
%track_sine; 
%track_wall; 
%load splineTrajectory
track_stadium; 

W = 15; %Trackwidth
%[leftWall] = generateWalls(x_track, y_track, W, "left"); 
%[rightWall] = generateWalls(x_track, y_track, W, "right"); 
% close all; 
% plot(x_track, y_track, 'LineWidth',2); 
% hold on ;
% plot(leftWall.x, leftWall.y, 'k--'); 
% plot(rightWall.x, rightWall.y, 'r--');
% axis equal
% xlabel('X[m]'); 
% ylabel('Y[m]')
% legend('Base spline', 'Left projection', 'Right projection');

lookAhead = 3; 

%% Wall sensors
%Add a distance sensor set to sense the wall, which here is represented by
%the Ya axis. See the report in ../../Documentation for a graphical view of
%it. 

%Put it on the upper right corner of the car. 
xs_f = -B/2; 
ys_f = L; 
alpha_s_f = deg2rad(45); 

wallSensorParams_front = Simulink.Bus.createMATLABStruct('wallSensor');

wallSensorParams_front.xs = xs_f; 
wallSensorParams_front.ys = ys_f;
wallSensorParams_front.alpha_s = alpha_s_f; 
wallSensorParams_front.ds_min = 1; 

xs_b = -B/2; 
% ys_b = 0;
ys_b = L/2; 
% alpha_s_b = deg2rad(-10);
alpha_s_b = deg2rad(-30);

wallSensorParams_rear= Simulink.Bus.createMATLABStruct('wallSensor');
wallSensorParams_rear.xs        = xs_b; 
wallSensorParams_rear.ys        = ys_b;
wallSensorParams_rear.alpha_s   = alpha_s_b; 
wallSensorParams_rear.ds_min = 1;


%Update frequency of kalman filter. 
Ts_kalman = 0.005; 
Ts_distSensor = 0.005; 


%% Simulation parameters
%pos_noise_power = 0.1^2; %Simulate noise in position estimate. 
heading_noise_power = deg2rad(1)^2; 
% heading_noise_power =0; 
%heading_noise_power =0; 
pos_noise_power =0.5^2; 

Ts_GPS = 1/50; 
%Sampling times 
Ts_tachometer = 0.001; 

%% Simple P- controller
%Try and control orientation towards the wall using a simple P controller

% Kp_wall = 0.03; 
Kp_wall = 0.09;
Ki_wall = 0;
% Kd_wall = 5;
d_set =  10; 
ds_set = d_set/(cos(wallSensorParams_front.alpha_s)); %Project d to ds, for when running on front sensor P controller only. 

%% Simulate 

%Init state vector. States are pos x, pos y, heading angle, all in global
%coords. 
%x_init = zeros(3,1); 
x_init = [12,45,deg2rad(40)]; 
%x_init = [20; -15; deg2rad(-90)]; 

sim('kinematicModel');

%% Animation
animateVehicle; 

%% Plots 
postProcessing; 




