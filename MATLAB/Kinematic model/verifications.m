

close all;
clear all; 
clc; 

%Loop through a few points and make sure the pure pursuit does what it is
%supposed to. 

track_straightLine; 

L = 5; %Length between front wheel axis 
B = 2.2; %Track width

lookAhead = 30; 

heading = [0; -pi/9 ; -pi/6 ; pi/3 ; -pi/3]; 
testPos = [0,0 ; 
           5, 10;
           30, -15;
           60, 15 ; 
           70, -20]; 

testFig = figure;
for i = 1:length(heading)
    
    currentPos = testPos(i,:)'; 
    currentHeading = heading(i); 
    
    [closest,j] = findClosestWaypoint(trajectory, currentPos);
    target = findNextTargetWaypoint(trajectory, currentPos, lookAhead); 
    [testSteeringAngle, arc] = getSteeringAngle( currentPos,target, currentHeading,L  );

    l1 = plot(x_track, y_track, 'b*', 'MarkerSize',1.5); 
    hold on;
    l2 =plot(testPos(1), testPos(2), 'kx')
    l3 = plot(closest(1), closest(2), 'ro')
    l4 =plot(target(1), target(2), 'm*')
    l5 = plot(arc(:,1), arc(:,2), 'k');
    plotCar(testFig, currentPos, currentHeading, B,L, testSteeringAngle, testSteeringAngle ); 

end 


legend([l1,l2,l3,l4,l5],{'Trajectory', 'Current pos', 'Closest waypoint', 'Target waypoint', 'Pure pursuit arc'})
axis equal
