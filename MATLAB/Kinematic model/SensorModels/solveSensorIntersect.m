close all;
clear all; 

%Solve intersection between a straight line from the sensor running from
%[Sx,Sy] to second order polynomial parametrized on t. 

syms p3x p2x p1x 
syms p3y p2y p1y 
syms t ds Sx Sy ex ey

%Equations for interserction in x,y
X =  p3x*t^2 +p2x*t - ds*ex  == Sx -p1x;
Y = p3y*t^2 +p2y*t  - ds*ey == Sy -p1y; 

eqns = [X;Y]; 

S = solve(eqns, [t, ds] );

t_sol = simplify(S.t);
ds_sol = simplify(S.ds);

%% Generate a matlab function
%Polynomial on the form returned from matlabs polyfit. 
x_poly = [p3x p2x p1x]; 
y_poly = [p3y p2y p1y]; 

sensorUnitVec = [ex; ey]; 
sensorPos = [Sx; Sy]; 

%Do not generate this every time, at least not if you just changed
%anything. 
% matlabFunction([t_sol] ,        'Comments', 'Function for calculating intersection between sensor vector and cubic line parametrized on t.',...
%                                 'File', 'sensorIntersect_getT',...
%                                 'Vars',{x_poly, y_poly,sensorPos, sensorUnitVec},...
%                                 'Outputs',{'t'})
                            
                            
%This can be generated too, but we actually only need t so.                             
% matlabFunction([ds_sol] , 'Comments', 'Function for calculating intersection between sensor vector and cubic line parametrized on t.',...
%                           'File', 'sensorIntersect_getDs',...
%                           'Vars',{x_poly, y_poly,sensorPos, sensorUnitVec},...
%                            'Outputs',{'ds'})



%[t_sol,ds_sol] = 