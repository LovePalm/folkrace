
function xYandDs =  wallAndSensorIntersection(X0, x_interPol, y_interPol, vectorFromGlobalOriginToSensorOrigin, psi, beta_s)

    %This is a part of the wall sensor model that had to be lifted out due
    %to  problems with extrinsic function calls. 
    %Extrinsic functions cannot pass function handles over call boundaries
    %in Simulink so we need to have all function handles here. 
    %https://se.mathworks.com/matlabcentral/answers/16798-coder-and-error-passing-handles-to-extrinsic-functions#answer_22663
    %
    %
    % Anyway, this is what it does. In a previous step a "decent" 
    % intersection between a sensor read and a point along the wall was
    % found. 
    % 
    % In order to improve accuracy a bit, interpolate between the previous
    % decent intersection, and neigbouring points along the wall. 
    % @param[in] x0 - Initial guess 
    % @param[in] x_interPol - Interpolation of wall, x
    % @param[in] y_interPol - Interpolation of wall, y
    % @param[in] vectorFromGlobalOriginToSensorOrigin - A vector describing
    %                                                   the sensor origin in the world frame. 
    % @param[in] psi - Current heading
    % @param[beta_s] - Sensor angle w.r.t to vehicle frame Xb axis. 
    % @param[out] - Intersection point (x,y) on wall and sensor value ds 
    
    %Fit adjacent points to a polynomial of n-th order 
    poly = polyfit(x_interPol,y_interPol,2); 
    
    %Function handle for evaluating the polynomial function. 
    polyNomialFitToWallCurve = @(x)[ x ; 
                                    polyval(poly, x)]; 
                                
    %Describe the unit vector running paralell to the sensor "beam". 
    %We do this be defining the vector in the vehicle frame and then rotate
    %it to the world frame. 
    sensorUnitVector = rotBtoA(psi)*[ cos(beta_s); 
                                          sin(beta_s)  ]; 
                
    sensorEquation = @(ds) vectorFromGlobalOriginToSensorOrigin(:) + ds*sensorUnitVector; 
    
   
    
    fun = @(X ) (   polyNomialFitToWallCurve(X(1))  - sensorEquation(X(2))   );
    
    %Specifiy bonds in wherein we search for a solution. Hmm starts to look more and more like fmincon. 
    lb = [x_interPol(1); 0]; 
    ub = [x_interPol(end); inf]; 
    
    xAndDs =  lsqnonlin(fun, X0, lb, ub);
    
    x_coord_opt = xAndDs(1); 
    ds = xAndDs(2); 
     
    y_coord_opt = polyval(poly, x_coord_opt); 
     
    xYandDs = [x_coord_opt, y_coord_opt, ds]; 
end 
