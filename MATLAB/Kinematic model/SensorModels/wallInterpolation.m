function [detectionPoint, ds] = wallInterpolation(wall, interpolationIndex, interpolPts, currentPos, heading, sensorParams)
%Say we have found a preliminary point along the wall for a sensor
%detection. We can use this function to interpolate between this
%preliminary point described by interpolationIndex and neighbouing points. 

     
    %Here we cover special conditions where we are close to end points. 
    lowerRange = interpolationIndex;
    upperRange = interpolationIndex; 
    
    %Force interpolation points to this currently. 
    interpolPts =1; 
    
    if(interpolationIndex -interpolPts <1)
        lowerRange = 1; 
        
    elseif(interpolationIndex +interpolPts > size(wall,1) )
        lowerRange = size(wall,1)-2*interpolPts; 
    else
        %If we have no range errors, interpolate symmetrically around the
        %provided interpolation index point. 
        lowerRange = interpolationIndex - interpolPts; 
    end
    
    upperRange = lowerRange + 2*interpolPts; 

    
    x_pts = wall(lowerRange:upperRange,1); 
    y_pts  = wall(lowerRange:upperRange,2); 
    
   
    %Map values between interpolation points 
    t = 0:1:length(x_pts)-1;
    t = t(:); 
    
    %Polynomial order. 
    Np = 2; 
    x_spline = polyfit(t,x_pts,Np); 
    y_spline = polyfit(t,y_pts,Np);
    
    beta_s = wrapTo2Pi( pi - sensorParams.alpha_s );
    %% Solve line intersection
    %Can be done analytically too, but we are lazy and do it using matlab. 
    
                                       
    wallSplineFit = @(t) [ polyval(x_spline,t); 
                           polyval(y_spline,t)  ];
                                              
    %Describe the unit vector running paralell to the sensor "beam". 
    %We do this be defining the vector in the vehicle frame and then rotate
    %it to the world frame. 
    sensorUnitVector = rotBtoA(heading)*[ cos(beta_s); 
                                          sin(beta_s)  ]; 
           
    [ds_vec, vectorFromGlobalOriginToSensorOrigin] = ...
        getSensorVector( wall( interpolationIndex, :)  ,currentPos, heading, sensorParams);
    
    %Option two: Use a general matlab function for this. Works, but very
    %slow in simulink.
%     sensorEquation = @(ds) vectorFromGlobalOriginToSensorOrigin(:) + ds*sensorUnitVector; 
%     
%     fun = @(X ) (   wallSplineFit(X(1))  - sensorEquation(X(2))   );
%     
%     ds = norm(ds_vec,2); 
%     X0 = [t(2);ds]; 
%     
%   options = optimoptions(@lsqnonlin,'Display','off',...
%                                     'OptimalityTolerance',0.3,...
%                                     'StepTolerance',0.01,... 
%                                     'MaxIterations',10);
%                                 
%        %Specifiy bonds in wherein we search for a solution. Hmm starts to look more and more like fmincon. 
%     lb = [t(1);    0]; 
%     ub = [t(end);  inf]; 
%       
%     options = optimoptions(@lsqnonlin,'Display','off',...
%                                         'OptimalityTolerance',0.3,...
%                                          'StepTolerance',0.01,... 
%                                          'MaxIterations',10);
%     tAndDs =  lsqnonlin(fun, X0, lb, ub, options);
% %                             
% %     %Recover detecion point from result. 
% %    


  %% Try analytical solution
  lala =0; 
  
   t_candidates = ...
          sensorIntersect_getT(x_spline, y_spline, vectorFromGlobalOriginToSensorOrigin(:)   , sensorUnitVector(:)  );
    
    %t_candidates = [0;0]; 

    %Assume detection point outside of our defined interpolation ranges are
    %invalid. Complex solution means the sensor center line can never intersect
    %the track, but we detected a track point in the previous step due to
    %the sensor spread width. 
    
    if(isempty(t_candidates) )
        validPoint = false; 
    else 
        %
        validPoint = (imag(t_candidates)==0) &...
                      (t_candidates  >= t(1)) &...
                      (t_candidates <= t(end)); 
    end
  
     
    %Check that we have a valid point, but assure we only have one.
    %According to the geometrical intepretation of this interpolation we
    %should not have two intersections. 
    if(any(validPoint) && sum(validPoint)==1 )
        t_candidates = t_candidates(:); 
        t_opt = t_candidates(validPoint); 
        detectionPoint = wallSplineFit(t_opt); 
    else
         %Else return original point. 
         detectionPoint= wall(interpolationIndex,:); 
    end
    
    %Recover sensor vector using linear algebra. 
    ds_vec  =detectionPoint(:)-vectorFromGlobalOriginToSensorOrigin(:);
    ds = norm(ds_vec,2); 
    
    detectionPoint = detectionPoint(:)'; 

    %%%% Debug plots. 
%     close all;
%     
%     %Do a precision interpolation for visualisation. 
%     t_prec = linspace(t(1),t(end), 1000); 
%     
%     x_interpol = polyval(x_spline, t_prec); 
%     y_interpol = polyval(y_spline, t_prec);
%     
%     prelDetectionPoint = wall(interpolationIndex,:); 
%    
%     l1 = plot(wall(:,1), wall(:,2), 'ko'); 
%     hold on; 
%     l2= plot(x_pts, y_pts, 'r*'); 
%     l3 = plot(x_interpol, y_interpol,'m','LineWidth',2); 
%     l4 = plot(prelDetectionPoint(1),prelDetectionPoint(2) , 'k+', 'MarkerSize',15); 
%  
%     l5 = plot(detectionPoint(1), detectionPoint(2), 'square', 'LineWidth',2);
%     %xYandDs = [x_coord_opt, y_coord_opt, ds]; 
%     
%     B = 2;
%     L =5;
%  
%     plotCar(l1, currentPos, heading, B, L, 0,0); 
%     plotWallSensor(ds, heading, currentPos, sensorParams)
%     axis equal; 
%     
%     xlabel('$x_A$');
%     ylabel('$y_A$');
%     
%    set(gca,'xticklabel',[])
%     set(gca,'yticklabel',[])
%    
%     
%     legend([l1,l2,l3,l4,l5],{'Wall points','Interpolation points',...
%             'Spline interpolation', ...
%              'Detection point from previous step', ...
%              'Interpolated point'});
%          
%     lala =0; 

end

