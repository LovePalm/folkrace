close all;
clear all; 
clc; 

%%The distance sensor model 

addpath(genpath('../')); 
load 'busDefinitions'

%Vehicle dimensions. 
B = 2; 
L = 5; 
%% Choose a trajectory 
load testTrajectory;

%track_wall; 

%Generate some random vehicle positions. 
dist = 3; 
bias = 1; 


%Remove some samples so we dont get too many samples in the plots just
Kd = 2; %Put larger then unity for downsampling.
downSample = 1:Kd:length(x_track); 

x_track = x_track(downSample); 
y_track = y_track(downSample); 

vehicle_x = x_track + rand(size(x_track))*dist + bias; 
vehicle_y = y_track + rand(size(x_track))*dist + bias;

heading = rand(size(x_track))*2*pi; 


currentPos = [vehicle_x(:), vehicle_y(:)];

%% Define sensor 
%Put it on the upper right corner of the car. 
xs_f = -B/2; 
ys_f = L; 
alpha_s_f = deg2rad(27); 

wallSensorParams_front = Simulink.Bus.createMATLABStruct('wallSensor');

wallSensorParams_front.xs = xs_f; 
wallSensorParams_front.ys = ys_f;
wallSensorParams_front.alpha_s = alpha_s_f; 

%% Simulate sensor
wall = [x_track(:), y_track(:)]; 
currentPos = [vehicle_x(:),vehicle_y(:)]; 

%Try one point for starters

x0 = 25; 
x1 = 50; 
x_pos = linspace(x0,x1,5); 
y0 =0;  
%x_pos  = x0:1:x0*4; 
y_pos  = ones(size(x_pos))*y0; 
currentPos  = [x_pos(:), y_pos(:)]; 
heading = ones(size(x_pos))*deg2rad(0); 
% currentPos = [x0,   10 ;
%              x0+1, 10, 
%              x0+2, 10,
%               34, 15,
%               50, 15, 
%               50, 15,
%               50, 15,
%               50, 15]; 
% heading = [deg2rad(-20),deg2rad(-20),deg2rad(-20),deg2rad(-45), deg2rad(-90), deg2rad(-180), deg2rad(-270), deg2rad(10)] ; 

N = length(heading); 
detectionPoint = zeros(N,2); 
ds = zeros(N,1); 
for i = 1:N 
    [detectionPoint(i,:), ds(i)] = distanceSensorModel(currentPos(i,:), heading(i), wallSensorParams_front, wall  );
end



%% Plots 
close all; 
set(0,'defaultAxesYGrid','on');
set(0,'defaultAxesXGrid','on');
set(0,'defaulttextInterpreter','latex')
  set(groot, 'DefaultLegendInterpreter', 'latex')
fig = plot(x_track, y_track,'ko', 'LineWidth',2);


hold on; 
steering =zeros(size(heading)); 

plotCarRep(fig, currentPos, heading,  B, L, steering, steering); 
l1 = plot(currentPos(:,1), currentPos(:,2), 'bo');
 plotWallSensor(ds, heading, currentPos, wallSensorParams_front); 
l2 = plot(detectionPoint(:,1), detectionPoint(:,2), 'rx')
legend( [fig(1), l1(1), l2(1)], {'Wall points','Vehicle coordinate system origins $\mathbf{O}_b$', 'Wall detection points'} )

axis equal; 

xlabel('$x_a$')
ylabel('$y_a$')