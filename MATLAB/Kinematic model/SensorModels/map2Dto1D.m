function [point, id] = map2Dto1D(points2D, coord1D, startId, maxPoints)
% Say we have a curve, like a sine, and want to find a point on this track defined by a 1D coordinate,
% which is the line integral of the sine. Start on a point startId on points2D.
% then go down or up the path depending on the specified coord1D. We approximate the line integral by 
% by a cumlative sum over the norms of each point. 

%TODO: Implement the maxPoints function too. 

if(coord1D >0)
 cumD   = cumsum( sqrt( diff( points2D(startId:end,1) ).^2  +...
                        diff( points2D(startId:end,2) ).^2  ) );
              
              
elseif (coord1D <0)
 cumD   = cumsum( sqrt( diff( points2D(startId:-1:1,1) ).^2  +...
                        diff( points2D(startId:-1:1,2) ).^2 )  ); 
end

%This implies the user sent a value corresponding to the endpoints. 
%Return the index that was provided (being end or startpoint)
if(isempty(cumD))
    id = startId; 
    point = points2D(startId,:); 
else
    %The order of summation in the cumulative sum temporaily flip the sign. 
    %of what is positive direction down the 1D path. So, we have to use 
    %absolute value for it here. 
    [~, indexOffs] = min( ( cumD- abs(coord1D) ).^2  ); 

    %Indexoffs returned from min is at least 1
    id = startId + sign(coord1D)*(indexOffs); 
    point = points2D(id,:); 
end
end

