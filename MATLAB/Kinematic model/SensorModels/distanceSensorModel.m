function [detectionPoint, ds] = distanceSensorModel(Pos, heading, sensorParams, wall)
%Given a currente vehicle position Pos, a distance sensor 
%defined by sensorParams and parameters for a wall, find where the distance
%sensor will intersect the wall. 
%@param Pos - Position as columnvectors [x,y]
%@param heading - Heading in radians
%@param sensorParams - Struct containing sensor placement parameters
%@param wall        - Column vector describing points along the wall we are
%                     detecting [xw, yw] 


% if(length(heading)~=size(Pos,1))
%  error("Distance sensor model: Length of position and heading vector are inequal."); 
% end
 
ds =0; 
detectionPoint = [0,0]; 
%The sensor angle is wr.t to negative part of the vehicle yxb axis 
%which is retrospect is not very neat, should have it wrt the xb axis 
beta_s = wrapTo2Pi( pi - sensorParams.alpha_s );

psi = heading; 

ds_hat = getSensorVector(wall, Pos, heading, sensorParams ); 

sensorAngles_fun = @(ds_hat)(  wrapTo2Pi( atan2( ds_hat(:,2), ds_hat(:,1) ) - psi  )   );
sensorAngles     =  sensorAngles_fun(ds_hat); 
 
 %% Remove infeasible sensor angles. 
 % Sensor angle can only be close to the nominal sensor angle. The angular
 % tolerance creates a cone where we continue our search for a nominal
 % point. A too large cone will include to many elements to a cone too
 % small will exclude some. 
    
angleTol = deg2rad(6); 

%Remember angles have to wrapped to the same angular range for this to be
%relevant! Angle tol is a cone defined by +/- angleTol around a zero
%reference line, being beta_s. So angles should be in range +/- pi for this to work.
elementsInSearchCone =  abs(wrapToPi(sensorAngles - beta_s)) < angleTol; 

% %If we didnt find any on the first search, do a wider search.
% if( ~(elementsInSearchCone) )
%     angleTol  = deg2rad(10);
%     elementsInSearchCone = abs(wrapToPi(sensorAngles - beta_s)) < angleTol;
% end

  
if(any(elementsInSearchCone))

    %Points that are within our cone. The cone is defined by angleTol. 
    wall_red    = wall(elementsInSearchCone, :); 
    ds_hat_red  = ds_hat(elementsInSearchCone,:); 

    %% Criterion 1: Pick the point closest to the sensor in search cone
    %Picking the closest element effectively cancels out any readings 
    %"through" a wall
    ds_candidates_sqr = ds_hat_red(:,1).^2  + ds_hat_red(:,2).^2 ;
    [~,k] = min( ds_candidates_sqr); 
    
    ds_prel = sqrt(ds_candidates_sqr(k)); 
    prelDetectionPoint = wall_red(k,:); %For debug mostly.
    
    ds = ds_prel; 
    detectionPoint = prelDetectionPoint; 
    
    %% Criterion 2: Pick element layin approximately on an arc in the cone. 
    % Check the report to see a visualization of this. The prev. criterion
    % might have given us an element towards the ends of the cone, while we
    % want one in the middle. 
    
    %If we have very few elements in the current search range, do not
    %bother. 
        
    indexes = find(elementsInSearchCone,k); 

    restoredIndex = indexes(end); %So this is where we find the current preliminary index on the track. 

    %Find interpolation range. Interpolate over a distance that is an arc
    %over the cone, with some margin. 

    interPoldist = (2*angleTol*ds_prel*1.1);

    prelMaxSearchRange = 30; %So we do not search aaaaall points every time.   

    %TODO: If we were to know if the point is before or after our
    %preliminary point due to some criterion, we could center our search
    %more accureately. This works however, we just have to search wider. 
    [~, upperRange] = map2Dto1D(wall,  interPoldist, restoredIndex, prelMaxSearchRange ); 
    [~, lowerRange] = map2Dto1D(wall, -interPoldist, restoredIndex, prelMaxSearchRange );

    %Search in the reduced vector, and return an offset with respect to the
    %lowerRange value. 
    [~,k_offs]    = min(   (   sensorAngles(lowerRange:upperRange) -beta_s).^2); 

    k_offs = k_offs-1; 
    
    refinedIndex = lowerRange+k_offs; 

    ds = norm(ds_hat(refinedIndex, :),    2); 
    detectionPoint =  wall(refinedIndex, :);
    lala =0; 

    %% Interpolation
    %%% The wall is defined by a discrete set of points. We can increase
    %%% resolution a bit by applying interpolation between points. 
   
    %The interpolation uses a quadratic polynomial, parametrize on an auxiliary variable t.
    %Interpolation is done between between the current
    %detectionpoint (called refinedIndex here) and the previous and
    %following point along the wall. Does improve resolution a lot. 
    %
    %Currently, the intersection is found analytically. This function is probably fast enough that we could
    %use it to to loop over a whole track, instead of using the
    %previous two steps in this sensor funcion. 
    [detectionPoint_interPol, ds_interPol] = wallInterpolation(wall, refinedIndex ,interPoldist, Pos, heading, sensorParams);
    
    detectionPoint = detectionPoint_interPol; 
    ds = ds_interPol; 
    
%     close all;  
%     figHandle = plot(wall(:,1), wall(:,2), 'k--'); 
%     hold on;
%     plot(wall(elementsInSearchCone,1), wall(elementsInSearchCone,2), 'rx');
%     %figHandle = plot(x_interPol_pts, y_interPol_pts, 'ro'); 
%     hold on; 
%     
%       
%     plot(prelDetectionPoint(1),prelDetectionPoint(2) , 'kx'); 
%     
%     plot( wall(lowerRange:upperRange,1),wall(lowerRange:upperRange,2),'ro' )
%  
%     plot(detectionPoint(1), detectionPoint(2), 'b*');
%     %xYandDs = [x_coord_opt, y_coord_opt, ds]; 
%     legend('Wall points','Elements in cone','Prel. detection from distance criterion','Refined search range', 'Refined detection point point');
%     B = 2;
%     L =5;
%  
%     plotCar(figHandle, Pos, heading, B, L, 0,0); 
%     plotWallSensor(ds, psi, Pos, sensorParams)
%     %Plot the cone where we detect values. 
%     s1 = sensorParams; 
%     s2 = sensorParams; 
%     s1.alpha_s = sensorParams.alpha_s -angleTol; 
%     s2.alpha_s = sensorParams.alpha_s +angleTol; 
%     plotWallSensor(ds, psi, Pos, s1,'r')
%     plotWallSensor(ds, psi, Pos, s2,'r')
%     axis equal; 
%     lala =0; 


else
    detectionPoint = [0, 0]; 
    ds = 0; 
end

end %End of first function



