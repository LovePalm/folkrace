function [ds_vec, vectorFromGlobalOriginToSensorOrigin]  = getSensorVector(points,Pos, heading, sensorParams)

%Given a certain position and heading, return all vectors spanning from
%sensor origin to points, that are cartesian points as row vectors. This is
%provided that the sensor is assumed to rotate freely temporarily. 

xs = sensorParams.xs; 
ys = sensorParams.ys; 
    
 %Brute force approach. Try all points of the wall, and see what the
 %resulting angle between the x_b axis of the vehicle and the sensor read
 %vector is. 
 
 psi = heading; 
 
 %See how the vector from the vehicle coordinate origin Ob to the sensor origin
 %appears in the global frame 
 sensorVecInGlobalFrame = rotBtoA(psi)*[xs;ys];
 
  %We check the angle between the vector going from the sensor position to
 %all points of the wall, and the vehicle frame axis. The one closest to
 %the nominal sensor angle the one we choose as the detection point. 
 %Use repmat for the substraction to all wall points. 
 vectorFromGlobalOriginToSensorOrigin = Pos(:)'+ sensorVecInGlobalFrame(:)';  
 
 ds_vec = points - repmat(vectorFromGlobalOriginToSensorOrigin, length(points),1);

end

