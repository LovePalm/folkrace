close all; 
animationWindow = figure; 

FPS = 25; 
T_FPS = 1/25; 

%Assume data collected at uniform time intervals. 
Ts_data = mean(diff(pos_x_true.Time)); 
%Downsample when there is a lot of data so animation is not too slow. Plot 
%every K'th sample. 
K = T_FPS/Ts_data; 

K = round(K); 

 N = length(pos_x_true.Data)
for i = 1:K:N-1
    
    if~ishandle(animationWindow)
        break; 
    end
    
    %% Update position etc
    currentPos = [pos_x_true.Data(i), pos_y_true.Data(i)];
    psi = heading_true.Data(i); 
    ds_front = wallSensor_front.Data(i); 
    ds_rear = wallSensor_rear.Data(i); 

    %% Plot  car
    %Use quiver arrows for this. 
    leftSteeringAngle = leftSteering.Data(i); 
    rightSteeringAngle = rightSteering.Data(i);
    
    plotCar(animationWindow, currentPos, psi, B,L, leftSteeringAngle, rightSteeringAngle  ); 
    
    %TODO: Wall sensor should be an object with a plot function perhaps? 
    plotWallSensor(ds_front, psi, currentPos, wallSensorParams_front); 
    plotWallSensor(ds_rear, psi, currentPos, wallSensorParams_rear); 
    
    %% Plot path so far
    %TODO: Probably more effecient using animated  line here. 
    plot(pos_x_true.Data(1:i), pos_y_true.Data(1:i), 'k--'); 
    
    %% Plot setpoint trajectory 
    plot(x_track, y_track, 'ro')
       
    %% Plot settings 
    hold off
    box = 40; 
    %axis equal
%     xmin = min([pos_x_true.Data(1:i); -10]);
%     xmax = max([pos_x_true.Data(1:i); 10]); 
%     ymin = min([pos_y_true.Data(1:i);-10]);
%     ymax = max([pos_y_true.Data(1:i)+20;10]); 

    xmin = pos_x_true.Data(i)-box; 
    xmax = pos_x_true.Data(i)+box; 
    ymin = pos_y_true.Data(i)-box; 
    ymax = pos_y_true.Data(i)+box;
    
    axis( [ xmin, xmax, ymin, ymax]); 
    axis equal
    %grid on; 
   % showaxes('on')
    drawnow
    pause( 1/FPS   );   

end
