function  plotCarRep(figHandle, currentPos, heading, B,L, leftSteeringAngle, rightSteeringAngle)
%Plot a car to figure in figHandle. This is a wrapper for calling 
%plotCar function repetitive times. Is is not vectorized or optimized 
% as this function is mostly used for just plotting multiple vehicles 
% for debug purposes offline. 

%@param currentPos - Car position defined as column vectors x,y 
%@param heading    - Heading in radians
%@param B          - Trackwidth
%@param L          - Length axle to axle 
%@param steeringAngle - The current steering angles, to vizualize the
%                       wheels angles. 
%

if( size(currentPos,1) ~= length(heading)   )
   error('Incompatible matrix dimensions for heading an position ') 
end

for i = 1:length(heading)
   plotCar(figHandle, currentPos(i,:), heading(i), B, L, leftSteeringAngle(i), rightSteeringAngle(i)); 
end

end

