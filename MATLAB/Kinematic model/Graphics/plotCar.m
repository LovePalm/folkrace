function  plotCar(figHandle, currentPos, heading, B,L, leftSteeringAngle, rightSteeringAngle)
%Plot a car to figure in figHandle
%@param currentPos - Car position defined as column vector x,y 
%@param heading    - Heading in radians
%@param B          - Trackwidth
%@param L          - Length axle to axle 
%@param steeringAngle - The current steering angles, to vizualize the
%                       wheels angles. 
%

if( size(currentPos) ~= [1,2]    )
   error('Incompatible matrix dimensions for position argument, should be a row vector. ') 
end

%Define car rectangle at origin with heading =0 using two vectors. 
v1_0 = [0; L]; 
v2_0 = [B; 0]; 

%% Plot car body
    %Rotate vectors describing car in car frame B to global frame A 
    v1 = rotBtoA(heading)*v1_0; 
    v2 = rotBtoA(heading)*v2_0; 

    %The vehicle point of reference is in the middle of the vector v2
    P0 = currentPos(:) - v2/2; 
    
    %Corners of rectangle of vehicle. 
    C = [P0 , P0+v1 , P0+v1+v2 , P0+v2 , P0]'; 
    plot(C(:,1), C(:,2), 'r')
    % plot(C1(:,1),C1(:,2),'r')
    hold on
    %Plot car coordinate system origin 
    plot(currentPos(1),currentPos(2), 'ko')
    
    %% Plot front wheel direction vectors. 
    wheelSize = 1; %Parameter just for describing wheel size, for wheel vectors. 
    theta = heading; 
    
    leftWheelPos = P0+v1; 
    rightWheelPos = P0+v1+v2; 
    leftWheelOffs = wheelSize*rotBtoA(theta)*[-sin(leftSteeringAngle);cos(leftSteeringAngle) ];
    rightWheelOffs = wheelSize*rotBtoA(theta)*[-sin(rightSteeringAngle);cos(rightSteeringAngle) ];
   
    %Quiver is origin for first 2 arguments, and then vector definition
    quiver(leftWheelPos(1),leftWheelPos(2),...
           leftWheelOffs(1),leftWheelOffs(2),0   ,'k'  )
    quiver(rightWheelPos(1),rightWheelPos(2),...
           rightWheelOffs(1),rightWheelOffs(2),0 ,'k'    )
    
    


end

