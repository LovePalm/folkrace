function plotWallSensor(ds, psi, currentPos, params, color)
%Given a sensor measurement ds, current heading psi, and sensor location
%parameters, plot sensor in the a plot window assumed to be open already. 
%
%The "wall" is the y-axis in a cartesian 2D plot. 
%param ds           - Sensor distance, scalar vector
%param psi          - Heading, vector i radians 
%param currentPos   - Position, column vectors [x,y]
%param Params       - Struct with sensor parameters. 

if(nargin <= 4)
   color = 'k';  
end

xs = params.xs;
ys = params.ys; 
alpha_s = params.alpha_s; 


%% Plot distance sensor 
    %Get location of the sensor in the world frame. 
for i = 1:length(psi)
    sensorOrigin = currentPos(i,:)' + rotBtoA(psi(i) )*[xs;ys]; 
    sensorVec = rotBtoA(psi(i))*[-cos(alpha_s);sin(alpha_s)]*ds(i); 
    
     %Quiver is origin for first 2 arguments, and then vector definition
    quiver(sensorOrigin(1),sensorOrigin(2),...
           sensorVec(1),sensorVec(2),0   ,color  ); 
end
   
end

