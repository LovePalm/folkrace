function [target, j] = findNextTargetWaypoint(trajectory, currentPos, lookAhead)
%Given a certain lookAhead distance and a current point on the trajectory i
%find the next target.

%Find the waypoint closest to the current position 
[startWaypoint, ~, i] = findClosestWaypoint(trajectory,currentPos); 

%Find the distances to the other waypiints to the start waypint
distances = sqrt((trajectory(:,1)-startWaypoint(1)).^2+(trajectory(:,2)-startWaypoint(2)).^2); 

%Look at waypoints further ahead from the current. 
[~, j] = min( abs(distances(i:end)-lookAhead)); 

j = j+i-1; 

%TODO: We could interpolate between waypoints to find something closer to
%the lookahead. 

target = trajectory(j,:)'; 


end

