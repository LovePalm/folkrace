function [steeringAngle, arc] = getSteeringAngle(currentPos, targetPos, heading, L)
%Get current steering angle for the inner wheel for pure pursuit. Arc is
%debugging info, an arc going from the vehicle origin to the target. 

%TODO: Handle saturation on steering angle! 

deltaP = targetPos-currentPos; 

%Pure pursuit it derived in the vehicle coordfinate system. 
%https://www.ri.cmu.edu/pub_files/pub3/coulter_r_craig_1992_1/coulter_r_craig_1992_1.pdf

%Distance to we are from target 
d_sqr = deltaP(1)^2 + deltaP(2)^2;  

deltaP_B = rotAtoB(heading)*deltaP; 

%Target pos expressed in body frame B 
x_t = deltaP_B(1); 
y_t = deltaP_B(2); 

x_tol = 0.01; 

%Pure pursuit formulates an arc curvature going from our current position
%to the goal position. If the target is traight ahead of us in our body
%coordinates, we set the steering angle straight and go straight forward, 
if abs(x_t) > x_tol
    %Arc radius. 
    R = d_sqr/(2*x_t); 
    
    %The steering angle is mapped in such a way that we need to reverse
    %sign. Arguably atan2 should be used, however atan caps the output to
    %the theoretical max feasible range -pi/2 - pi/2
    steeringAngle = atan(-L/R); 
   
    %steeringAngle  = atan2(-R,L); 
    %Debug: Calculate arc from vehicle origin to target point. 
   %TODO: Do a parametrized curve instead
%    d = R-x_t; 
%     %alpha = atan2(y_t,d );
%     alpha = acos(d/abs(R)); 
%     %Determines the locus dirction 
%     if(x_t>0)
%          alpha_vec = -pi:0.01:alpha; 
%     else
%         alpha_vec = 0:0.01:alpha;
%     end
%    
%     x_arc = R*(1+cos(alpha_vec)); 
%     y_arc = R*sin(alpha_vec); 
    
    x_arc = 0:0.01:x_t;
    y_arc = sqrt(R^2 - (x_arc-R).^2); 
    y_arc = real(y_arc); %Only values > 0 in coordinate system. 

    %TODO: Does not work in simulink=?? 
%      %Rotate from coordinate system in body B to world frame A 
%     arc = rotBtoA(heading)*[x_arc ; 
%                            y_arc]; 
%                        
%     %Add current origin B
%     arc = arc + currentPos(:); 
%     arc = arc'; 
%    
else
    steeringAngle =0; 
    
end 


end

