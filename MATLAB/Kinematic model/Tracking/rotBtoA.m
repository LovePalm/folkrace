function rotBtoA_mat = rotBtoA(psi)

%Get the matrix rotating a vector from the vehicle coordinate sytem B
%to the global coordinate system A. Psi is the current heading  

rotBtoA_mat =    [cos(psi), -sin(psi);... 
                  sin(psi), cos(psi)  ];  
end

