function [rotAtoB_mat] = rotAtoB(psi)

%Use the relation of tranposes for rotation matrices. 
rotBtoA_mat = rotBtoA(psi);
rotAtoB_mat = rotBtoA_mat'; 
end

