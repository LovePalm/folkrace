function [closest, distances, i] = findClosestWaypoint(waypoints, pos)
%Find the closest waypoint, euclidean definition. And at what index i it is found. 
%Trajectory is an xy trajectory with x in column i and y in column 2


% Calculate all distances 
delta_x = waypoints(:,1)-pos(1); 
delta_y = waypoints(:,2)-pos(2); 
distances = sqrt(delta_x.^2 +delta_y.^2); 

[~,i]= min(distances); 
closest = waypoints(i,: )'; 



end

