
close all;

set(0,'defaultAxesYGrid','on');
set(0,'defaultAxesXGrid','on');

set(0,'defaultAxesYGrid','on');
set(0,'defaultAxesXGrid','on');
set(0,'defaulttextInterpreter','latex')
  set(groot, 'DefaultLegendInterpreter', 'latex')

plot(pos_x_true.Data, pos_y_true.Data)
hold on; 
plot(x_track, y_track, 'ko')
plot(pos_x_meas.Data, pos_y_meas.Data)
title('Position')
legend('True position', 'Nominal trajectory', 'Measured position')

axis equal


%%%Time domain
figure; 
m =2; 
n =2; 

subplot(m,n,1); 
trueColor = 'k'; 
measColor = 'r';
plot(pos_x_true.Time, pos_x_true.Data, trueColor)
hold on; 
plot(pos_x_meas.Time, pos_x_meas.Data, measColor)
xlabel('Time[s]')
legend('True x', 'Mesured x');
title('Position'); 

subplot(m,n,2); 
plot(pos_y_true.Time, pos_y_true.Data, trueColor)
hold on; 
plot(pos_y_meas.Time, pos_y_meas.Data, measColor)
xlabel('Time[s]')
legend('True y', 'Mesured y');
title('Position'); 

subplot(m,n,3); 
stairs(tacho_rear_left.Time, tacho_rear_left.Data);
hold on; 
stairs(tacho_rear_right.Time, tacho_rear_right.Data);
xlabel('Time[s]'); 
ylabel('[m]'); 
title('Tachometer readings'); 
legend('Rear left tachometer', 'Rear right tachometer'); 

figure; 
m = 1; 
n = 3; 
subplot(m,n,1); 
plot(heading_true.time, rad2deg( wrapTo2Pi(heading_true.Data)));
hold on; 
plot(heading_meas.time, rad2deg( wrapTo2Pi(heading_meas.Data)));
title('Heading')
xlabel('Time'); 
ylabel('Heading [deg]')
legend('True heading $\psi$', 'Measured heading $\hat\psi$')

subplot(m,n,2);
plot(set_innerSteer.time, rad2deg(set_innerSteer.Data)); 
title('Inner steering angle')
xlabel('Time'); 
ylabel('[deg]')
legend('$\delta_i$')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%Wallsensors 
figure ;
stairs(wallSensor_front.Time, wallSensor_front.Data)
hold on; 
stairs(wallSensor_rear.Time, wallSensor_rear.Data)
title('Wall/Distance sensors'), 
legend('Front sensor', 'Rear sensor');
xlabel('Time[s]'); 
ylabel('Distance [m]')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% State estimates 
figure; 
m = 1; 
n  =2; 
subplot(m,n,1); 
stairs(EKF_d.Time, EKF_d.Data); 
legend('Estimated perpendicular distance to wall tangent $\hat{d}$')
xlabel('Time[s]'); 
ylabel('Distance [m]'); 
hold on; 
subplot(m,n,2); 
stairs(heading_true.Time, rad2deg(wrapToPi(heading_true.Data)));
hold on; 
stairs(EKF_theta.Time, rad2deg(wrapToPi(EKF_theta.Data)));
legend('True heading $\psi$','Estimated angle $\hat{\theta}$ to wall tangent. ')
xlabel('Time[s]');
ylabel('Angle[deg]')
