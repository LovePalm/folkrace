
function [wall] = generateWalls(x_track, y_track, W, leftRight)

%TODO: Parametrize splines separately for x,y using a third parameter t? 
%Generate a spline fit to a 2D trajectory. From this spline fit, walls are projected.
%@param[in]  x_track Track coordinates, which to generate the wall around 
%@param[in]  w       Width of the track .
%@param[in]  leftRight String specifier if the wall should be left or right
%            of the track. 
%@param[out] leftWall Output wall stored as two column vectors x,y
%

%% Rotate track to avoid singularities 
%The incoming tracks often have vertical and horizontal line which 
%create singularities when doing a spline fit. In order to dodge this
%problem we rotate the track a little bit. 

alpha = deg2rad(0); %TODO: Has some flaw. 

track_rot = rotAtoB(alpha)*[x_track(:)'; y_track(:)']; 

x_track = track_rot(1,:); 
y_track = track_rot(2,:); 

pp = spline(x_track,y_track )

%Differentiate polynomial to get tangent, then invert to get normal to tangent. 

pp_k = ppDer(pp); 

%We calculate the tangent of the curve simply by differentiating the polynomial d
%describig the curve w.r.t x. The normal is then calculated as standard, but we 
% we need to sign flip it as the normal will change direction when the derivative does otherwise. 
k = ppval(pp_k, x_track);  %Tangent of original curve. 
k_t = -1./k; 

%Reverse delta_x to put wall on left or right side of the curve we build
%the wall around. As default we put it right of the curve. 
delta_x  = W./sqrt(1+k_t.^2).*sign(k);    %Tangent of normal.; 

if(leftRight == "left")
    delta_x = -delta_x; 
end
    
delta_y = k_t.*delta_x; 

%Should be w
normError = sum( sqrt(delta_x.^2+ delta_y.^2)-W)

%%%Rotate our track back to the original frame and return a struct. 
leftWall_mat = rotAtoB(-alpha)*[x_track(:)' + delta_x(:)';
                                y_track(:)' + delta_y(:)']

wall.x  = leftWall_mat(1,:); 
wall.y = leftWall_mat(2,:); 

end 