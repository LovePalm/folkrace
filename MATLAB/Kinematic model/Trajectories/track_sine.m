%Distance between each x coordinate point. 
d = 0.4; 
k = 0.1; 
x_track = 0:d:200; 
y_track = 5*sin(x_track/20)

trajectory = [x_track(:), y_track(:) ]; 