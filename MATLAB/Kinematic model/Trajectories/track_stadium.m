
%Nominal distance between each waypoint
d = 0.1; 
%Generate a track that looks like a "stadium" 

base = 100; 
radius = 30; 

y1 = 0:d:base; 
x1 = zeros(size(y1)); 

%%% Create the half circle on top. 

%Use solution to the circle equation
% x2 = 0:d:radius*2; 
% y2 = sqrt(radius^2 - (x2-radius).^2) + base; 

%Use parametrized curve, and polar coordinates. Gives evenly spaced points

t = linspace(pi,0, radius*pi/d);
x2 = radius*cos(t)+radius; 
y2 = radius*sin(t)+base; 

%%%Create the right hand side
y3 = base:-d:0; 
x3= ones( size(y3))*2*radius; 


k = 0.1; 
x_track = [x1,x2,x3];  
y_track = [y1, y2,y3];  

close all
%plot(x2, y2)
hold on; 
plot(x_track, y_track, 'ro')
axis equal
trajectory = [x_track(:), y_track(:) ]; 