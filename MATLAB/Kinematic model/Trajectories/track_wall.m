%Distance between each wp
d = 0.2; 
k = 2; 

%We simulate a wall as a line parallell to the y-axis. 
 
y_track = 0:d:100; 
x_track = 0*ones(size(y_track)); 

trajectory = [x_track(:), y_track(:) ]; 