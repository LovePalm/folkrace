
clear all;
clc; 
close all; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Takes in clicks on a 2d map and generates a position trajectory from this
%using a parametrized curve and cubic splines. 
%
%By http://mechatronicsblogger.blogspot.com, 
%
%Martin P�r-Love Palm , from the Follow Me project 2017
%
%https://MarzCoder@bitbucket.org/MarzCoder/followme.git

%% SETTINGS 

Np = 10; %Number of user input points 

%Generate true trajectory at this distance inverval [meters]
delta_d = 2; 

%Size of plot window
xmax = 100; 
ymax = 100; 

coordFig = figure; 
axis([0, xmax, -0, ymax]);
grid on; 
title('Click to generate trajectory'); 
xlabel('meters'); 
ylabel('meters'); 

userInputCoords = zeros(0,2); 

%% Wait for the user to input a number of points
for i = 1:Np
    
   [x,y] = ginput(1);
   
   userInputCoords = [userInputCoords; [x y]];
   
   h(1) = plot(x,y,'kx'); 
   axis([0, xmax, 0, ymax]);
   grid on
   hold on; 
   
end 

%% Do spline interpolation between points of interest 
x = userInputCoords(:,1); 
y = userInputCoords(:,2); 

%Get length of paramtrized curve
%https://se.mathworks.com/matlabcentral/newsreader/view_thread/33868
d_user = cumsum(sqrt([0,diff(x')].^2 + [0,diff(y')].^2));

dd = min(d_user):delta_d:max(d_user); 

yy =  spline(d_user,y,dd);
xx =  spline(d_user,x,dd);

h(2) = plot(xx, yy);  

legend(h,'User inputs','Generated trajectory'); 

%% Save trajectory

dd          = dd; %distance from start 
x_true      = xx; 
y_true      = yy; 

x_track = xx;
y_track = yy; 

trajectory = [xx(:), yy(:)];
%save('splineTrajectory.mat', 'trajectory', 'x_track', 'y_track');  

%save( 'splineTrajectory.mat','d_user','dd','x_true','y_true','delta_d');