%See if the EKF filter is observable etc. 

psi = deg2rad(20); 
alphas = alpha_s; 
Ts = 0.005; 
v = 1; 

A  =[ 1 , -Ts*v*cos(psi),   0 ,     0;
      0,    1,              Ts,     0;
      0,    0,              1,      0;
      0,    0,              0,      1  ]; 
  
%States are d, psi, psi_dot
A2 = [ 1 ,  0,              0 ;
      0,    1,              Ts;
      0,    0,              1;];
  
%states d, psi, psi_dot, d_dot
A3  =[   1 ,   0,    0 ,     Ts;
        0,    1,    Ts,     0;
        0,    0,    1,      0;
        0,    0,    0,      1  ]; 

H = [1/cos(alphas-psi), -1/cos(alphas-psi).^2.*(ys.*cos( ...
alphas)+xs.*sin(alphas)+d.*sin(alphas+(-1).*psi)),0,0]; 

H2 = H(1:3);
H3 = H; 

%States are d, psi, measurements are ds  
A5 = [1 0; 
      0 1]; 
  
H4 = H(1:2); 

%States are d, psi, psi_dot and measurements are ds and psi_dot
%(gyroscope)

%States are d, psi,psi_dot measurements are ds  
A5 = [1 0 0; 
      0 1 Ts
      0 0 1]; 
H5 = [H(1:3); 0,0,1]; 

%States are d, psi, psi dot and we have two linearly independent sensors. 
A6 = [1 0 0 ; 
      0 1 Ts; 
      0 0 1 ]; 
H6 = [2 5 0 ; 
      4 7 0 ]; 

obsmat = obsv(A6,H6); 
rank(obsmat)