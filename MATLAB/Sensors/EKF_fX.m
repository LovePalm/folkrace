function f = EKF_fX(in1,Ts)
%EKF_FX
%    F = EKF_FX(IN1,TS)

%    This function was generated by the Symbolic Math Toolbox version 8.3.
%    11-Jun-2019 13:27:29

%Non. lin prediction function f(x)
d = in1(1,:);
theta = in1(2,:);
f = [d;theta];
