function H = EKF_getH(in1,in2)
%EKF_GETH
%    H = EKF_GETH(IN1,IN2)

%    This function was generated by the Symbolic Math Toolbox version 8.3.
%    11-Jun-2019 13:27:30

%Jacobian of non. lin meaurement function H(x)
alpha_s = in2(3,:);
d = in1(1,:);
theta = in1(2,:);
xs = in2(1,:);
ys = in2(2,:);
t2 = -theta;
t3 = alpha_s+t2;
H = [1.0./cos(t3),-(ys.*cos(alpha_s).*2.0+d.*sin(t3).*2.0+xs.*sin(alpha_s).*2.0)./(cos(alpha_s.*2.0-theta.*2.0)+1.0)];
