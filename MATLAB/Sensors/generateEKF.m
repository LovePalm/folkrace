
close all; 
clear all; 

%% Generate an EKF using wall sensors to estimate distance and heading to a straight wall. 

syms d theta  theta_dot v    %State vector, distance to wall, heading, derivative of heading and velocity component in body frame. 
syms xs ys  alpha_s Ts     %Sensor params and aux variables 

%% State vector 
%x = [d; theta; theta_dot; v]; 
x = [d; theta]; 
params = [xs;ys;alpha_s; Ts]; 

%% Prediction step
%f_d         = d -Ts*v*sin(theta); %Is a projection of velocity 
f_d         = d; %Constant velocity model
%f_theta       = theta + Ts*theta_dot;  
f_theta = theta; %Constant velocity model
f_theta_dot   = theta_dot;          %Constant velocity model for angular velocity
f_v         = v; 

%Non-lin state update function. 
%f = [f_d; f_theta; f_theta_dot; f_v]; 
f = [f_d; f_theta]; 

% Export non linear prediction function to file. 
matlabFunction(f ,'File', 'EKF_fX','Vars',{x, Ts}, 'Comments','Non. lin prediction function f(x)');

%Jacobian for update step (equivalent to the A state space transition
%matrix in a linear kalman filter) 
A = jacobian(f, x)


%To generate a file for jacobian prediction matrix. 
matlabFunction(A ,'File', 'EKF_getA','Vars',{x, Ts});

%% Measurement update 
%Here we only measure ds

%This function is derived in the report. 
f_ds = ( d+xs*cos(theta)-ys*sin(theta))/(cos(alpha_s-theta)); 

H = simplify(jacobian(f_ds, x)) 

matlabFunction(H, 'File', 'EKF_getH', 'Vars',{x, params},... 
               'Comments','Jacobian of non. lin meaurement function H(x)' );
           
matlabFunction(f_ds, 'File', 'EKF_getDs', 'Vars',{x, params},... 
   'Comments','Non. lin measurement function H(x)' );

%ccode(H) %Generate C code to create the H matrix. 
%latex(H) %For report integration. 


%% Observability 
%Create dummy values and see if the linearized system i observable. 

x_num = rand(length(x),1);

%Add more sensors with random parameter. 
param_num_1 = rand(length(params),1);
param_num_2 = rand(length(params),1);

Ts_num = 0.005; 
A_num = EKF_getA(x_num, Ts_num); 
H_num_1 = EKF_getH(x_num, param_num_1); 
H_num_2 = EKF_getH(x_num, param_num_2);

H_num = [H_num_1;H_num_2];
%H_num = H_num_1; 
rankObsv = rank(obsv(A_num, H_num));  

if (  rankObsv == length(x))
    disp('Observable!!'); 
else 
    disp('Not observable')
end 

