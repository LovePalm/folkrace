clc;
clear all;
close all; 

%Fit a curve to measurements taken manually from a Sharp distance sensor
%and a white reflective surface 

%Distance in meters and ADC value
d = [0.05, 0.08, 0.1,0.12,0.14,0.16,0.18,0.2,0.25, 0.3, 0.4,0.5]; 
val = [657,695,590,485,425,380,342,317,262,230,187,163]; 


%% Polynomial fit 

order = 3; 
p = polyfit(val,d,order ); 

val_fit = 0:1:1023; 
d_fit = polyval(p, val_fit); 

%We cannot have a distance <0
val_fit = val_fit(d_fit>=0); 
d_fit = d_fit(d_fit >=0); 

%Make sure to CAP the ADC reading in the uc to this, in order to not get
%infeasible results
val_max = max(val_fit)


%% Figures 
close all
plot(val, 1000*d, 'ko')
hold on;
plot(val_fit, 1000*d_fit, 'r'); 
ylabel('Distance [m.m]'); 
xlabel('ADC value'); 
legend('Samples', 'Fitted curve')
grid on; 
